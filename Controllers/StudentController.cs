using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetMVC.Models;
using dotnetMVC.Services;
using Microsoft.AspNetCore.Mvc;

namespace dotnetMVC.Controllers
{
    public class StudentController : Controller
    {
        private IStudentService _studentService;
        private IClassService _classService;
        public StudentController(IStudentService studentService, IClassService classService)
        {
            _studentService = studentService;
            _classService = classService;
        }
        public IActionResult Index()
        {
            var students = _studentService.GetStudents();
            return View(students);
        }

        public IActionResult Detail(int id)
        {
            var student = _studentService.getStudentById(id);
            return View(student);
        }

        public IActionResult Edit(int id)
        {
            var student = _studentService.getStudentById(id);
            var classes = _classService.GetClasses();
            ViewBag.classes = classes;
            return View(student);
        }
    }
}