using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetMVC.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnetMVC.Services
{
    public interface IStudentService
    {
        List<Student> GetStudents();
        Student getStudentById (int id);
    }
    public class StudentService : IStudentService
    {
        private DataContext _context;
        public StudentService(DataContext context)
        {
            _context = context;
        }
        public List<Student> GetStudents()
        {
            return _context.Students.Include(x => x.Class).ToList();
        }
        public Student getStudentById(int id) 
        {
            return _context.Students.FirstOrDefault(x => x.StudentId == id);
        }
    }

}