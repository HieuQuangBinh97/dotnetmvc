using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetMVC.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnetMVC.Services
{
    public interface IClassService
    {
        List<Class> GetClasses();
        Class getClassById (int id);
    }
    public class ClassService : IClassService
    {
        private DataContext _context;
        public ClassService(DataContext context)
        {
            _context = context;
        }
        public List<Class> GetClasses()
        {
            return _context.Classes.ToList();
        }
        public Class getClassById(int id) 
        {
            return _context.Classes.FirstOrDefault(x => x.ClassId == id);
        }
    }

}